# Auto GitLab Backup B2 Ansible Role
This is a role for Ansible to ensure backups of GitLab are made to Backblaze B2.

## Variables
|             Name               |                         Description                                  | Required |        Default            |
| ------------------------------ | -------------------------------------------------------------------- | -------- | ------------------------- |
| `b2_account_id`                | Your Backblaze B2 account ID (or application ID) which you can find by [https://secure.backblaze.com/b2_buckets.htm](logging in to Backblaze and creating a new application ID.)  |    ✔️     |   N/A |
| `b2_application_key`           | Your Backblaze B2 application ID which you can find by [https://secure.backblaze.com/b2_buckets.htm](logging in to Backblaze and creating a new application ID.) |    ✔️     |   N/A |
| `gitlab_backup_hour`           | What hour to create the backup in local time. |    ❌    | `4` |
| `gitlab_backup_b2_bucket_name` | The B2 bucket to store your backups to.       |    ❌    | `gitlab-{{ ansible_hostname }}` |
| `auto_gitlab_backup_source`    | Where to install `auto-gitlab-backup` from    |    ❌    | `https://github.com/sund/auto-gitlab-backup.git` |
| `auto_gitlab_destination_path` | Where to install `auto-gitlab-backup` to      |    ❌    | `/opt` |

Note: `auto-gitlab-backup` does not back up your secrets and does not encrypt your files on Backblaze.

You can use this role in your site playbook like so:

## Usage

```yaml
- hosts: all
  remote_user: root
  tasks:
  - include_role:
      name: gitlab-backups-b2
    notify:
        - restore from backups
    vars:
       b2_account_id: abcdef1234
       b2_application_key: abcdef1234
```
You could move the notify to another point in your playbook.

## License

AGPLv3
